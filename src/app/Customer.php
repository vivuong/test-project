<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $guarded = [];
    protected $attributes = [
        'active' => 1,
    ];

    public function getActiveAttribute($atrribute){
        return $this->activeOptions()[$atrribute];
    }

    public function company()
    {
        return $this->belongsto(Company::class);
    }

    public function activeOptions(){
            return [
                1 => "Active",
                0 => "Inactive",
                2 => "in-Progress",
            ];
    }
}
