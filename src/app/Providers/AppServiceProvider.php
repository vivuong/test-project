<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Billing\PaymentGateway;
use App\Billing\PaymentGatewayContract;
use App\Billing\CreaditGateway;
use App\Company;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PaymentGatewayContract::class ,function ($app){
            if( request()->has('credit') ) 
                return new CreaditGateway('usd');
            return new PaymentGateway('usd');
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['customers.index'],function($view){
            $view->with('companies',Company::all());
        });
    }
}
