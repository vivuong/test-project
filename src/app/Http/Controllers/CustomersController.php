<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Company;
use Intervention\Image\Facades\Image;
/**
 * Class PackageController
 *
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class CustomersController extends Controller
{
    

    public function __construct(){
        $this->middleware('auth')->except(['index']);
    }

    public function index(){
        //$customers = Customer::leftJoin('companies','companies.id','=','customers.company_id')->paginate(50);
        $customers = Customer::with('company')->paginate(50);
        //dd($customers);
        //$companies = Company::all();

        return view('customers.index',[
            'customers' => $customers,
            //'companies' => $companies,
        ]);
    }

    public function create(){
        $companies = Company::all();
        $customer = new Customer();

        return view('customers.create',compact('companies','customer'));
    }

    public function store(){
        $this->authorize('create',Customer::class);

        $customer = Customer::create($this->validateRequest());
        $this->storeImage($customer);
        //$customer = new Customer();
        //$customer->name = request('name');
        //$customer->email = request('email');
        //$customer->save();

        return redirect('customers');
    }

    public function show(Customer $customer){
        return view('customers.show',compact('customer'));
    }

    public function edit(Customer $customer){
        $companies = Company::all();
        return view('customers.edit',compact('customer','companies'));
    }

    public function update(Customer $customer){
        $customer->update($this->validateRequest());
        $this->storeImage($customer);
        return redirect('customers/'.$customer->id);
    }

    public function destroy(Customer $customer){
        $this->authorize('delete',$customer);

        $customer->delete();

        return redirect('customers'); 
    }

    private function validateRequest(){
        return request()->validate([
            'name' => 'required|min:3',
            'email' => 'required|email',
            'active' => 'required',
            'company_id' => 'required',
            'image' => 'sometimes|file|image|max:1000',
            
        ]);
    }

    private function storeImage($customer){
        if(request()->has('image')){
            $customer->update([
                'image' => request()->image->store('uploads','public'),
            ]);
        }

        $image = Image::make(public_path('storage/'.$customer->image))->fit(300,300);
        //$image = Image::make(public_path('storage/'.$customer->image))->fit(300,300,null,'top-left');
        $image->save(); 
    }
}
