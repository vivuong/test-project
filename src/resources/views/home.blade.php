@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                        
                        {{-- <my-button text="my new text" type="submit"></my-button> --}}
                        --------------------------------------------------------------------
                        <h1>Components</h1>
                        <accordion v-for="item in items" :key="item.id" :item="item"></accordion>

                        --------------------------------------------------------------------
                        <h1>Computed Properties</h1>
                        <computed-properties></computed-properties>

                        --------------------------------------------------------------------
                        <h1>Outputting Data</h1>
                        <outputting></outputting>

                        --------------------------------------------------------------------
                        <h1>TODO - EXAMPLE</h1>
                        <simple-todo></simple-todo>

                        --------------------------------------------------------------------
                        <h1>V-IF - V-ELSE</h1>
                        <if-else></if-else>
                </div>
            </div>
        </div>
    </div>
    
@endsection
@section('vue')

@endsection