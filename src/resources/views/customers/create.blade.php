@extends('layouts.app')

@section('title')
Customers    
@endsection

@section('content')
<h1> Customers </h1>
<form action="/customers" method="post" enctype="multipart/form-data">
    @include('customers.form')
    <button type="submit" class="btn-primary">add customer</button>
    @csrf


</form>


@endsection