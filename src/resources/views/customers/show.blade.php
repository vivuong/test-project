@extends('layouts.app')

@section('title')
details customer    
@endsection

@section('content')
<h1> details custumer</h1>

<form action="/customers/{{ $customer->id }}" method="POST">
    @method('DELETE')
    @csrf
    <button type="submit" class="btn btn-danger">DELETE</button>
</form>

<p><a href="/customers/{{$customer->id}}/edit">EDIT</a></p>
<p><strong>name</strong> {{ $customer->name }}</p>
<p><strong>Email</strong> {{ $customer->email }}</p>
<p><strong>active</strong> {{ $customer->active }}</p>
<p><strong>Company</strong> {{ $customer->company->name }}</p>

@if($customer->image)
    <div class="row">
    <div class="col-12"><img src="{{ asset('storage/'.$customer->image) }}" alt="" class="img-thumbnail"></div>
    </div>
@endif

@endsection