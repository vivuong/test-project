@extends('layouts.app')

@section('title')
EDIT CUSTOMER    
@endsection

@section('content')
<h1> EDIT CUSTOMER </h1>
<form action="/customers/{{$customer->id}}" method="POST" enctype="multipart/form-data">
    @method('PATCH')
    @include('customers.form')
    <button type="submit" class="btn-primary">EDIT</button>
    @csrf


</form>


@endsection