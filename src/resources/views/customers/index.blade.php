@extends('layouts.app')

@section('title')
Customers    
@endsection

@section('content')
@can('create', App\Customer::class)
<p><a href="customers/create">and new customer</a></p>
@endcan

<h3>Customers</h3>
<ul>
    @foreach($customers as $customer)

        @can('view', $customer)
        <li><a href="/customers/{{$customer->id}}">{{ $customer->name }}</a> <span class="text-muted">{{ $customer->company->name}}</span></li>
        @endcan

        @cannot('view', $customer)
            <li>{{ $customer->name }} <span class="text-muted">{{ $customer->company->name}}</span></li>
        @endcannot
        
    
    @endforeach
</ul>
@if ($customers instanceof \Illuminate\Pagination\LengthAwarePaginator)
<div class="col-12 d-flex justify-content-center pt-4">
        {{ $customers->links() }}
</div>
@endif


 <div class="row">
    <div class="col-12">
        @foreach ($companies as $company)
            <h3>{{ $company->name }} </h3>
            <ul>
            @foreach ($company->customers as $customer)
                <li>{{ $customer->name }} {{$customer->active }}</li>
                
            @endforeach
            </ul>
        @endforeach
    </div>
</div>
@endsection